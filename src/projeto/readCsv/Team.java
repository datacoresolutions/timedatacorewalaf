package projeto.readCsv;

import java.util.ArrayList;
import java.util.List;

import projeto.enumerador.Status;
import projeto.exception.FormacaoDeTimesException;
import projeto.jogadores.Jogadores;
import projeto.util.TeamUtil;

public class Team {
 
	List<Jogadores> listaM = new ArrayList<>();
	List<Jogadores> listaF = new ArrayList<>();

	public void split(List<Jogadores> e) throws FormacaoDeTimesException {
//		Separando os jogadores  
 
		TeamUtil.SelecionarGol(e);
		TeamUtil.DefStatus(e);
//		for (int i = 0; i < e.size(); i++) {
//			if (e.get(i).getSex() == 'F' && e.get(i).getStatus().equals(Status.Titular) || e.get(i).getStatus().equals(Status.Reserva)) {
//				e.get(i).setTimes(Time.timeFeminino);
//			} else if(e.get(i).getSex() == 'M' && e.get(i).getStatus().equals(Status.Titular) || e.get(i).getStatus().equals(Status.Reserva)){
//				e.get(i).setTimes(Time.timeMasculino);
//			} 
//		}

		TeamUtil.excecoes(e);
	}

	public static double score(List<Jogadores> lista) {
		double totalM = 0;
		double totalF = 0;
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).getSex() == 'M' && lista.get(i).getStatus() == Status.Titular
					|| lista.get(i).getStatus() == Status.Reserva) {
				totalM += (lista.get(i).getDribbles() + lista.get(i).getKicks() + lista.get(i).getPasses());

			} else if (lista.get(i).getSex() == 'F' && lista.get(i).getStatus() == Status.Titular
					|| lista.get(i).getStatus() == Status.Reserva) {
				totalF += (lista.get(i).getDribbles() + lista.get(i).getKicks() + lista.get(i).getPasses());

			}

		}
		for (int i = 0; i < TeamUtil.goleiros.size(); i++) {
			if (TeamUtil.goleiros.get(i).getSex() == 'M' && TeamUtil.goleiros.get(i).getStatus() == Status.Titular
					|| TeamUtil.goleiros.get(i).getStatus() == Status.Reserva) {
				totalM += TeamUtil.goleiros.get(i).getDefense();
			} else if (TeamUtil.goleiros.get(i).getSex() == 'M'
					&& TeamUtil.goleiros.get(i).getStatus() == Status.Titular
					|| TeamUtil.goleiros.get(i).getStatus() == Status.Reserva) {
				totalF += TeamUtil.goleiros.get(i).getDefense();
			}

		}
		System.out.println("Total Masculino: " + totalM);
		System.out.println("Total Feminino: " + totalF);
		return totalM + totalF;
	}
//	Get e Set

	public List<Jogadores> getListaM() {
		return listaM;
	}

	public List<Jogadores> getListaF() {
		return listaF;
	}
}
