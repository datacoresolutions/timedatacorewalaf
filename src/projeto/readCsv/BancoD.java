package projeto.readCsv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import connection.pacage.Conexao;
import projeto.jogadores.Jogadores;
import projeto.util.TeamUtil;

public class BancoD {

	public static void registrarBd(List<Jogadores> lista) {
		Connection con = Conexao.ObterConexao();
//		adicionando time masculino
		for (int i = 0; i < lista.size(); i++) {
			String sql = "INSERT INTO joga(nome, sobrenome, sexo, idade, chutes, passes, dribles, defesa, media, status, salario, time) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			try {
				PreparedStatement preparador = con.prepareStatement(sql);
				preparador.setString(1, lista.get(i).getName());
				preparador.setString(2, lista.get(i).getLastName());
				preparador.setString(3, String.valueOf(lista.get(i).getSex()));
				preparador.setInt(4, lista.get(i).getAge());
				preparador.setDouble(5, lista.get(i).getKicks());
				preparador.setDouble(6, lista.get(i).getPasses());
				preparador.setDouble(7, lista.get(i).getDribbles());
				preparador.setDouble(8, lista.get(i).getDefense());
				preparador.setDouble(9, lista.get(i).media());
				preparador.setInt(10, lista.get(i).getStatus().getValor());
				preparador.setDouble(11, lista.get(i).getSalary());
				preparador.setInt(12, lista.get(i).getTimes().getValor2());

				preparador.execute(); 
				preparador.close();
				System.out.println("Time masculino formado");
			} catch (SQLException e2) {
				e2.printStackTrace();
				System.out.println("N�o foi possivel formar o time masculino");
			}
		}
//			Adicionando Goleiros
 
		for (int i = 0; i < TeamUtil.goleiros.size(); i++) {
			String sql = "INSERT INTO joga(nome, sobrenome, sexo, idade, chutes, passes, dribles, defesa, media, status, salario,time) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			try {
				PreparedStatement preparador = con.prepareStatement(sql);
				preparador.setString(1, TeamUtil.goleiros.get(i).getName());
				preparador.setString(2, TeamUtil.goleiros.get(i).getLastName());
				preparador.setString(3, String.valueOf(TeamUtil.goleiros.get(i).getSex()));
				preparador.setInt(4, TeamUtil.goleiros.get(i).getAge());
				preparador.setDouble(5, TeamUtil.goleiros.get(i).getKicks());
				preparador.setDouble(6, TeamUtil.goleiros.get(i).getPasses());
				preparador.setDouble(7, TeamUtil.goleiros.get(i).getDribbles());
				preparador.setDouble(8, TeamUtil.goleiros.get(i).getDefense());
				preparador.setDouble(9, TeamUtil.goleiros.get(i).media());
				preparador.setInt(10, TeamUtil.goleiros.get(i).getStatus().getValor());
				preparador.setDouble(11, TeamUtil.goleiros.get(i).getSalary());
				preparador.setInt(12, TeamUtil.goleiros.get(i).getTimes().getValor2());

				preparador.execute();
				preparador.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}

		} 
	}

}
