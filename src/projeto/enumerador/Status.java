package projeto.enumerador;

public enum Status {
	 
	Reserva(2,"Reserva"),
	Titular(1,"Titular"),
	NaoJogador(0, "");
	
	private int valor;
	private String desc;
	
	private Status(int valor, String desc) {
		this.valor = valor;
		this.desc = desc;
	} 
	
	public int getValor() {
		return this.valor;
	}

	public String getDesc() {
		return this.desc;
	}
	
}
