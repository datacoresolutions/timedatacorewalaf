package projeto.enumerador;

public enum Time {
	
	timeMasculino(1),
	timeFeminino(2),
	naoPossuiTime(3);
	
	private Time(int valor2) {
		this.valor2 = valor2;
	}

	private int valor2;

	public int getValor2() {
		return valor2; 
	}

	public void setValor2(int valor2) {
		this.valor2 = valor2;
	}
	

}
