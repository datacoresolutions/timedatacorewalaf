package projeto.jogadores;

import java.util.ArrayList;
import java.util.Collections;

import projeto.enumerador.Status;
import projeto.enumerador.Time;

public class Jogadores {
	private String name;
	private String lastName;
	private int age;
	private char sex;
	private double kicks;
	private double dribbles;
	private double passes;
	private double defense;
	private Status status;
	private double salary;
	private Time times;
	
	public double media() {
		return Math.round((this.getKicks() + this.getDribbles() + this.getPasses()) / 3);
	}

	public Jogadores(String name, String lastName, int age, char sex, double kicks, double dribbles, double defense,
			double passes) {
		this.name = name;
		this.lastName = lastName;
		this.age = age;
		this.sex = sex;
		this.kicks = kicks;
		this.dribbles = dribbles;
		this.defense = defense;
		this.passes = passes;
	}

	public Jogadores() {
	}

//	Get/Set
	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getKicks() {
		return kicks;
	}

	public void setKicks(double kicks) {
		this.kicks = kicks;
	}

	public double getDribbles() {
		return dribbles;
	}

	public void setDribbles(double dribbles) {
		this.dribbles = dribbles;
	}

	public double getPasses() {
		return passes;
	}

	public void setPasses(double passes) {
		this.passes = passes;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public double getSalary() {
		ArrayList<Double> lista = new ArrayList<>();
		lista.add(this.getKicks());
		lista.add(this.getPasses());
		lista.add(this.getDribbles());
		Collections.sort(lista, Collections.reverseOrder());

		if (this.getStatus().equals(Status.Titular)) {
			return Math.round(this.salary = (lista.get(0) + lista.get(1)) * 750);
		} else if (this.getStatus().equals(Status.Reserva)) {
			return Math.round(this.salary = ((lista.get(0) + lista.get(1)) * 750) / 2);
		} else {
			return Math.round(this.salary);
		}
	}
 
	public double getDefense() {
		return defense;
	}

	public void setDefense(double defense) {
		this.defense = defense;
	}
 
	@Override
	public String toString() {
		return "Nome: " + this.getName() + "\n" +
		"Sobrenome: " + this.getLastName() + "\n" +
		"Idade: " + this.getAge() + "\n" +
		"Sexo: " + this.getSex() + "\n" +
		"Chute: " + this.getKicks() + "\n" +
		"Passes: " + this.getPasses() + "\n" +
		"Drible: " + this.getDribbles() + "\n" +
		"Defesa: " + this.getDefense() + "\n" +
		"Media: " + this.media() + "\n" +
		"Status: " + this.getStatus() + "\n" +
		"Salario: " + this.getSalary() + "\n" + 
		"Time: " + this.getTimes();
	}

	public Time getTimes() {
		return times;
	}

	public void setTimes(Time times) {
		this.times = times;
	}

}
