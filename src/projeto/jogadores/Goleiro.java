package projeto.jogadores;

import projeto.enumerador.Status;

public class Goleiro extends Jogadores { 

	@Override
	public double getSalary() {
		if(this.getStatus().equals(Status.Titular)) {
		return (this.getDefense() * 1500);
		} else if(this.getStatus().equals(Status.Reserva)) {
			return(this.getDefense() * 1500)/2;
		}else {
			return 0;
		}
	}


}
