package projeto.exception;

public class FormacaoDeTimesException extends Exception {
	
	/** 
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FormacaoDeTimesException(String msg) {
		super(msg);
		System.out.println(msg);
	}
	

}
