package projeto.comparadores;

import java.util.Comparator;

import projeto.jogadores.Jogadores;

public class ComparadorDefesa implements Comparator<Jogadores>{

	@Override
	public int compare(Jogadores o1, Jogadores o2) {
		if (o1.getDefense() < o2.getDefense()) {
			return 1;
		} else if (o1.getDefense() > o2.getDefense()) {
			return -1;
	} else {
			return 0;
		}
	}
}
