package projeto.comparadores;

import java.util.Comparator;

import projeto.jogadores.Jogadores;

public class ComparadorMedia implements Comparator<Jogadores> {

	@Override
	public int compare(Jogadores o1, Jogadores o2) {
		if (o1.media() < o2.media()) {
			return 1;
		} else if (o1.media() > o2.media()) {
			return -1;
		} else {
			return 0;
		}
	}

}
