package projeto.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import projeto.comparadores.ComparadorDefesa;
import projeto.comparadores.ComparadorMedia;
import projeto.enumerador.Status;
import projeto.enumerador.Time;
import projeto.exception.FormacaoDeTimesException;
import projeto.jogadores.Goleiro;
import projeto.jogadores.Jogadores;

public class TeamUtil {
	public static List<Goleiro> goleiros = new ArrayList<>();

	public static void SelecionarGol(List<Jogadores> listaJogadores) {
		Collections.sort(listaJogadores, new ComparadorDefesa());
		int swapGolM = 2;
		for (int verIdade = 0; verIdade < listaJogadores.size(); verIdade++) {
			if (verIdade < 2 && listaJogadores.get(verIdade).getAge() > 23) {
				Collections.swap(listaJogadores, verIdade, swapGolM);
				swapGolM++;
			}
		}
		for (int i = 0; i < listaJogadores.size(); i++) {
			if (i == 0) {
				listaJogadores.get(i).setStatus(Status.Titular);
				Goleiro goleiroT = new Goleiro();
				goleiroT.setAge(listaJogadores.get(i).getAge());
				goleiroT.setName(listaJogadores.get(i).getName());
				goleiroT.setLastName(listaJogadores.get(i).getLastName());
				goleiroT.setSex(listaJogadores.get(i).getSex());
				goleiroT.setDefense(listaJogadores.get(i).getDefense());
				goleiroT.setDribbles(listaJogadores.get(i).getDribbles());
				goleiroT.setKicks(listaJogadores.get(i).getKicks());
				goleiroT.setPasses(listaJogadores.get(i).getPasses());
				goleiroT.setStatus(listaJogadores.get(i).getStatus());
				goleiroT.getSalary();
				if (listaJogadores.get(i).getSex() == 'M') {
					goleiroT.setTimes(Time.timeMasculino);
				} else {
					goleiroT.setTimes(Time.timeFeminino);
				}
				goleiros.add(goleiroT);
				listaJogadores.remove(listaJogadores.get(i));
			} else if (i == 1) {
				listaJogadores.get(i).setStatus(Status.Reserva);
				Goleiro goleiroR = new Goleiro();
				goleiroR.setAge(listaJogadores.get(i).getAge());
				goleiroR.setName(listaJogadores.get(i).getName());
				goleiroR.setLastName(listaJogadores.get(i).getLastName());
				goleiroR.setSex(listaJogadores.get(i).getSex());
				goleiroR.setDefense(listaJogadores.get(i).getDefense());
				goleiroR.setDribbles(listaJogadores.get(i).getDribbles());
				goleiroR.setKicks(listaJogadores.get(i).getKicks());
				goleiroR.setPasses(listaJogadores.get(i).getPasses());
				goleiroR.setStatus(listaJogadores.get(i).getStatus());
				goleiroR.getSalary();
				if (listaJogadores.get(i).getSex() == 'M') {
					goleiroR.setTimes(Time.timeMasculino);
				} else {
					goleiroR.setTimes(Time.timeFeminino);
				}

				goleiros.add(goleiroR);
				listaJogadores.remove(listaJogadores.get(i));
			}
		}

	}
 
	public static void excecoes(List<Jogadores> e) throws FormacaoDeTimesException {
		List<Jogadores> listaM = new ArrayList<>();
		List<Jogadores> listaF = new ArrayList<>();
		for (int i = 0; i < e.size(); i++) {
			if (e.get(i).getTimes().equals(Time.timeMasculino)) {
				listaM.add(e.get(i));
			} else if(e.get(i).getTimes().equals(Time.timeFeminino)){ 
				listaF.add(e.get(i)); 
			}
		} 
		if (listaM.size() < 6 || listaF.size() < 6) {
			throw new FormacaoDeTimesException("Um time precisa de pelo menos 8 pessoas ");
		}

		for (int i = 0; i < e.size(); i++) {
			if (e.get(i).getStatus().equals(Status.Titular) && e.get(i).getAge() > 23) {
				throw new FormacaoDeTimesException(
						"A idade de um dos jogadores e invalida, a idade dos jogadores precisa atender as regras Olimpicas.");
			}
		}
	} 

	public static void DefStatus(List<Jogadores> jogadores) {
		List<Jogadores> listaM = new ArrayList<>();
		List<Jogadores> listaF = new ArrayList<>();
		for(int i=0;i<jogadores.size();i++) {
			if(jogadores.get(i).getSex() == 'F') {
				listaF.add(jogadores.get(i));
			}else {
				listaM.add(jogadores.get(i));
			}
		}
		Collections.sort(listaM, new ComparadorMedia());
		int indexMasc = 6;
		for (int indexIdadeM = 0; indexIdadeM < listaM.size(); indexIdadeM++) {
			if (indexIdadeM < 6 && listaM.get(indexIdadeM).getAge() > 22) {
				Collections.swap(listaM, indexIdadeM, indexMasc);
				indexMasc++;
			}
		}
//	 	ListaM
		for (int i = 0; i < listaM.size(); i++) {
			if (i < 4) {
				listaM.get(i).setStatus(Status.Titular);
				listaM.get(i).setTimes(Time.timeMasculino);
			} else if (i >= 4 && i < 6) {
				listaM.get(i).setStatus(Status.Reserva);
				listaM.get(i).setTimes(Time.timeMasculino);
			} else {
				listaM.get(i).setStatus(Status.NaoJogador);
				listaM.get(i).setTimes(Time.naoPossuiTime);
			}
		}
//		ListaF
		Collections.sort(listaF, new ComparadorMedia());
		int indexFem = 6;
		for (int indexIdadeF = 0; indexIdadeF < listaF.size(); indexIdadeF++) {
			if (indexIdadeF < 6 && listaF.get(indexIdadeF).getAge() > 22) {
				Collections.swap(listaF, indexIdadeF, indexFem);
				indexFem++;
			}
		}
		for (int i = 0; i < listaF.size(); i++) {
			if (i < 4) {
				listaF.get(i).setStatus(Status.Titular);
				listaF.get(i).setTimes(Time.timeFeminino);
			} else if (i >= 4 && i < 6) {
				listaF.get(i).setStatus(Status.Reserva);
				listaF.get(i).setTimes(Time.timeFeminino);
			} else {
				listaF.get(i).setStatus(Status.NaoJogador);
				listaF.get(i).setTimes(Time.naoPossuiTime);
			}
		}
		
		

	}

}
