package projeto.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import projeto.exception.FormacaoDeTimesException;
import projeto.jogadores.Goleiro;
import projeto.jogadores.Jogadores;
import projeto.readCsv.Team;

public class CsvUtil {
	public static final int name = 0;
	public static final int lastName = 1;
	public static final int sex = 2;
	public static final int age = 3;
	public static final int kicks = 4;
	public static final int passes = 5;
	public static final int dribbles = 6;
	public static final int defense = 7;
 
	public static List<Jogadores> read() throws IOException, FormacaoDeTimesException {

		List<Jogadores> jogadores = new ArrayList<>();
		List<Path> files = new ArrayList<Path>();
		Scanner leitor = new Scanner(System.in);
		System.out.println("Digite o nome do arquivo");
		String nomeArch = leitor.next();
		leitor.close();
		String csv = "";

		files = Files.list(Paths.get("D:\\Development\\TimeDataCoreWalaf\\CSV\\"))
				.filter(path -> path.toFile().isFile() && path.toFile().getName().toLowerCase().endsWith("csv"))
				.collect(Collectors.toList());
		for (int file = 0; file < files.size();file++) {
			if (files.get(file).getFileName().toString().contains(nomeArch)) {
				csv = files.get(file).toString();
			}
		}

		Stream<String[]> lines = Files.lines(Paths.get(csv)).skip(1).map(line -> line.split(","));

		lines.forEach(line -> {
			String charSex = line[sex];
			if (charSex.contains("Fe") || charSex.contains("F") || charSex.contains("Mu")) {
				charSex = "F";
			} else {
				charSex = "M";
			}
			jogadores.add(new Jogadores(line[name], line[lastName], Integer.parseInt(line[age]), charSex.charAt(0),
					Double.parseDouble(line[kicks]), Double.parseDouble(line[dribbles]),
					Double.parseDouble(line[passes]), Double.parseDouble(line[defense])));
		});

//		jogadores.forEach(System.out::println);
//		gm.forEach(System.out::println);
		Team.score(jogadores);
		return jogadores;
	}

	public static void record(List<Jogadores> lm, List<Jogadores> lf) {

		Path path = Paths.get("c:/users/Walaf/desktop/brasiliam.txt");

		StringBuilder sb = new StringBuilder();

		sb.append("Nome").append(",").append("Sobrenome").append(",").append("idade").append(",").append("Sexo")
				.append(",").append("Chutes").append(",").append("Dribles").append(",").append("Passes").append(",")
				.append("Defesa").append(",").append("Media").append(",").append("Status").append(",").append("Salario")
				.append(",").append("\n");

		colocandoNoSb(lm, sb);
		colocandoNoSb(lf, sb);
		colocandoNoSbG(TeamUtil.goleiros, sb);
		try {
			BufferedWriter bw = Files.newBufferedWriter(path);
			bw.write(sb.toString());
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void colocandoNoSb(List<Jogadores> e, StringBuilder sb) {
		for (int i = 0; i < e.size(); i++) {
			sb.append(e.get(i).getName()).append(",").append(e.get(i).getLastName()).append(",")
					.append(e.get(i).getAge()).append(",").append(e.get(i).getSex()).append(",")
					.append(e.get(i).getKicks()).append(",").append(e.get(i).getDribbles()).append(",")
					.append(e.get(i).getPasses()).append(",").append(e.get(i).getDefense()).append(",")
					.append(e.get(i).media()).append(",").append(e.get(i).getStatus().getDesc()).append(",")
					.append(e.get(i).getSalary()).append("\n");
		}
	}

	private static void colocandoNoSbG(List<Goleiro> e, StringBuilder sb) {
		for (int i = 0; i < e.size(); i++) {
			sb.append(e.get(i).getName()).append(",").append(e.get(i).getLastName()).append(",")
					.append(e.get(i).getAge()).append(",").append(e.get(i).getSex()).append(",")
					.append(e.get(i).getKicks()).append(",").append(e.get(i).getDribbles()).append(",")
					.append(e.get(i).getPasses()).append(",").append(e.get(i).getDefense()).append(",")
					.append(e.get(i).media()).append(",").append(e.get(i).getStatus().getDesc()).append(",")
					.append(e.get(i).getSalary()).append("\n");
		}
	}
}