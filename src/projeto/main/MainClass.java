package projeto.main;
import java.io.IOException;
import java.util.List;

import projeto.exception.FormacaoDeTimesException;
import projeto.jogadores.Jogadores;
import projeto.readCsv.BancoD;
import projeto.readCsv.Team;
import projeto.util.CsvUtil;

public class MainClass {

	public static void main(String[] args) {

		try { 
			List<Jogadores> jogadores = CsvUtil.read();
			
			Team team = new Team();
			team.split(jogadores);
			CsvUtil.record(team.getListaM(), team.getListaF());
			BancoD.registrarBd(jogadores);
		} catch (IOException | FormacaoDeTimesException e) {
			e.printStackTrace();
		}
 	}
  
}
 