-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14-Ago-2018 às 15:41
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dc2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `joga`
--

CREATE TABLE `joga` (
  `Nome` varchar(20) NOT NULL,
  `Sobrenome` varchar(20) NOT NULL,
  `sexo` char(22) NOT NULL,
  `idade` int(22) NOT NULL,
  `chutes` float NOT NULL,
  `passes` float NOT NULL,
  `dribles` float NOT NULL,
  `defesa` float NOT NULL,
  `Media` float NOT NULL,
  `Status` int(22) NOT NULL,
  `Salario` float NOT NULL,
  `time` int(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `joga`
--

INSERT INTO `joga` (`Nome`, `Sobrenome`, `sexo`, `idade`, `chutes`, `passes`, `dribles`, `defesa`, `Media`, `Status`, `Salario`, `time`) VALUES
('Mario', 'Cezar', 'M', 17, 10, 9.3, 8.8, 10, 9, 1, 14475, 1),
('Neysea', 'Floor', 'M', 20, 10, 8.5, 10, 10, 10, 1, 15000, 1),
('Flavio', 'Augusto', 'M', 19, 10, 10, 10, 10, 10, 1, 15000, 1),
('Jorge', 'Seu', 'M', 15, 5.5, 4.4, 7.7, 9.8, 6, 0, 0, 3),
('Margarida', 'Zoe', 'F', 17, 4.4, 1.1, 9.9, 8.8, 5, 2, 5363, 2),
('Carlos', 'Joca', 'M', 20, 7.8, 6.6, 8.8, 8.8, 8, 1, 12450, 1),
('Maria', 'Teixeira', 'F', 16, 10, 10, 10, 8, 10, 1, 15000, 2),
('Carla', 'Santos', 'F', 58, 0, 4.4, 5.4, 6.9, 3, 0, 0, 3),
('Roger', 'Pessoa', 'M', 17, 4.4, 10, 8.8, 6.7, 8, 2, 7050, 1),
('Fagner', 'Meirelis', 'M', 33, 4.5, 6.6, 7, 5.6, 6, 0, 0, 3),
('Chris', 'Tiane', 'F', 13, 4.4, 7.7, 6.6, 5.5, 6, 1, 10725, 2),
('Matheus', 'Tornozelo', 'M', 17, 4.4, 6.21, 4, 5.5, 5, 0, 0, 3),
('Ana', 'Clara', 'F', 5, 6.6, 6.6, 5.5, 5.4, 6, 1, 9900, 2),
('Fabio', 'Rabin', 'M', 18, 10, 8, 1.7, 5.2, 7, 2, 6750, 1),
('Camila', 'Carmo', 'F', 17, 5, 6.6, 9, 4, 7, 1, 11700, 2),
('Rose', 'Angela', 'F', 15, 2.5, 10, 3.3, 3.1, 5, 0, 0, 3),
('Flavia', 'Santos', 'F', 25, 4.4, 8.8, 5.6, 0, 6, 0, 0, 3),
('Samila', 'Rodrigues', 'F', 22, 8.9, 10, 0, 0, 6, 2, 7087, 2),
('Marta', 'Souza', 'F', 22, 8, 9, 10, 10, 9, 1, 15000, 2),
('Janderson', 'Ulisses', 'M', 18, 1.2, 3.3, 10, 10, 5, 2, 7500, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `times`
--

CREATE TABLE `times` (
  `id` int(20) NOT NULL,
  `Nome` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `times`
--

INSERT INTO `times` (`id`, `Nome`) VALUES
(1, 'timeMasculino'),
(2, 'timeFeminino'),
(3, 'naoPossuiTime');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `times`
--
ALTER TABLE `times`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
